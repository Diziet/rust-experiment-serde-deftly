mod derive_de;
mod derive_ser;

/// Macro re-exports.
#[doc(hidden)]
pub mod m {
    pub use serde::{self, Deserialize, Serialize, __private::de as de_private};
    pub use std::{
        concat,
        default::Default,
        fmt,
        marker::PhantomData,
        option::Option::{self, None, Some},
        result::Result::{self, Err, Ok},
        string::String,
        stringify, write,
    };
}

// XXX Note on de_private
//    I am using private elements from serde::de::private, even though it isn't
//    stable, since my goal for now is to clone serde_derive.
//
//    Maybe later this should migrate to never use private, either by using
//    serde_value and friends instead, or by just copying some version of
//    serde::de::private, or by pinning it.
#[doc(hidden)]
pub use serde::__private::de as de_private;
#[doc(hidden)]
pub use serde::__private::ser as ser_private;

#[doc(hidden)]
pub use derive_deftly;
