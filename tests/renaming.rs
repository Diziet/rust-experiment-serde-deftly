use derive_deftly::Deftly;

#[derive(Debug, Clone, Deftly, Eq, PartialEq)]
#[derive_deftly(
    serde_deftly::Serialize,
    serde_deftly::Deserialize,
    test_utils::RealSerde
)]
#[deftly(serde(rename = "Enum"))]
#[deftly(real_serde = r#" #[serde(rename="Enum")] "#)]
enum AllSorts {
    #[deftly(serde(rename = "Tup"))]
    #[deftly(real_serde = r#" #[serde(rename="Tup")] "#)]
    Tuple(u32, u32),
    #[deftly(serde(rename = "Wrap"))]
    #[deftly(real_serde = r#" #[serde(rename="Wrap")] "#)]
    Newtype(u32),
    #[deftly(serde(rename = "Nil"))]
    #[deftly(real_serde = r#" #[serde(rename="Nil")] "#)]
    Unit,
    // XXXX Bug: this has to go last. derive-deftly#63.
    #[deftly(serde(rename = "Named"))]
    #[deftly(real_serde = r#" #[serde(rename="Named")] "#)]
    Struct {
        #[deftly(serde(rename = "alpha"))]
        #[deftly(real_serde = r#" #[serde(rename="alpha")] "#)]
        a: u32,
        #[deftly(serde(rename = "beta"))]
        #[deftly(real_serde = r#" #[serde(rename="beta")] "#)]
        b: u32,
    },
}

fn roundtrip(v: AllSorts) {
    test_utils::check_roundtrip::<AllSorts, AllSortsRealSerde>(v)
}

fn decode(s: &str) {
    test_utils::check_decoding::<AllSorts, AllSortsRealSerde>(s)
}

#[test]
fn rename_roundtrip() {
    roundtrip(AllSorts::Tuple(5, 10));
    roundtrip(AllSorts::Newtype(22));
    roundtrip(AllSorts::Unit);
    roundtrip(AllSorts::Struct { a: 4, b: 13 });
}

#[test]
fn rename_decoding() {
    decode(r#"{ "Tup": [4,13] }"#);
    decode(r#" "Nil" "#);
    decode(r#"{ "Wrap": 7 }"#);
    decode(r#"{ "Named": { "alpha": 10, "beta": 20 } }"#);
}
