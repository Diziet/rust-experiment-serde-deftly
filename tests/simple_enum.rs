use derive_deftly::Deftly;

#[derive(Debug, Clone, Deftly, Eq, PartialEq)]
#[derive_deftly(
    serde_deftly::Serialize,
    serde_deftly::Deserialize,
    test_utils::RealSerde
)]
enum AllSorts {
    Tuple(u32, u32),
    Newtype(u32),
    Unit,
    // XXXX Bug: this has to go last. derive-deftly#63.
    Struct { a: u32, b: u32 },
}

#[test]
fn r_unit() {
    test_utils::check_roundtrip::<AllSorts, AllSortsRealSerde>(AllSorts::Unit);
}

#[test]
fn r_newtype() {
    test_utils::check_roundtrip::<AllSorts, AllSortsRealSerde>(AllSorts::Newtype(413));
}

#[test]
fn r_tuple() {
    test_utils::check_roundtrip::<AllSorts, AllSortsRealSerde>(AllSorts::Tuple(4, 13));
}

#[test]
fn r_struct() {
    test_utils::check_roundtrip::<AllSorts, AllSortsRealSerde>(AllSorts::Struct { a: 4, b: 13 });
}
