use derive_deftly::Deftly;

#[derive(Debug, Clone, Deftly, Eq, PartialEq)]
#[derive_deftly(
    serde_deftly::Serialize,
    serde_deftly::Deserialize,
    test_utils::RealSerde
)]
struct Rgb {
    r: u8,
    g: u8,
    b: u8,
    #[deftly(serde(skip))]
    #[deftly(real_serde = r#" #[serde(skip)] "#)]
    x: u8,
}

#[test]
fn struct_with_skips() {
    test_utils::check_roundtrip::<Rgb, RgbRealSerde>(Rgb {
        r: 10,
        g: 11,
        b: 12,
        x: 0,
    });
    test_utils::check_decoding::<Rgb, RgbRealSerde>(r#" { "r":10, "g":11, "b":12 } "#);
}

#[derive(Debug, Clone, Deftly, Eq, PartialEq)]
#[derive_deftly(
    serde_deftly::Serialize,
    serde_deftly::Deserialize,
    test_utils::RealSerde
)]
struct Coord(
    u8,
    u8,
    #[deftly(serde(skip))]
    #[deftly(real_serde = r#" #[serde(skip)] "#)]
    u8,
);

#[test]
fn tuple_with_skips() {
    test_utils::check_roundtrip::<Coord, CoordRealSerde>(Coord(4, 13, 0));
    test_utils::check_decoding::<Coord, CoordRealSerde>("[ 4, 13 ]");
}
